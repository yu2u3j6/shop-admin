// 用于在项目中使用import.meta.env... 热更新相关API
/// <reference types="vite/client" />

// 给.vue结尾的资源做类型声明
declare module '*.vue' {
  import { DefineComponent } from 'vue'
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>
  export default component
}
