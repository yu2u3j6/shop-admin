import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'

import AppLayout from '@/layout/AppLayout.vue'
const routes = [
  {
    path: '/',
    component: AppLayout,
    children: [
      {
        path: '',
        name: 'home',
        component: () => import('../views/home/HomeIndex.vue')
      }
    ]
  },

  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login/LoginIndex.vue')
  }
] as RouteRecordRaw[]

export const router = createRouter({
  history: createWebHashHistory(),
  routes
})
