import { createApp } from 'vue'
import App from './App.vue'
import { router } from './router'
import { store } from './store'
import elementPlus from './plugins/element-plus'

import '@/styles/index.scss'
const app = createApp(App)
app.use(router)
app.use(store)
app.use(elementPlus)
app.mount('#app')

// import.meta.
