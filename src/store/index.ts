// src\store\index.ts
import { createStore } from 'vuex'

export const store = createStore({
  state: {
    count: 22
  },
  getters: {},
  mutations: {},
  actions: {},
  modules: {}
})
